## Descrição do projeto
O Senado Federal disponibiliza diversas APIs REST de dados abertos para que o público,
sendo que um dos endpoints descreve os Senadores em exercício atualmente.
Dado este endpoint (http://legis.senado.gov.br/dadosabertos/senador/lista/atual),
crie um modelo de dados que permita fácil acesso e manuseio a Senadores
em atividade e seus respectivos mandatos e popule os mesmos com os dados retornados pelo webservice.

## Técnologias
* Python 3.6.2
* [Peewee ORM](http://docs.peewee-orm.com/en/latest/)
* [Pytest](https://docs.pytest.org/en/latest/)


## Configurações
O arquivo **settings.py** é responsável por armazenar as variáveis de configuração do projeto,
como **url** base e dados do banco de dados.

## Ambientes

### Desenvolvimento
As bibibliotecas necessárias para o projeto foram isoladas com a utilização de 
[virtualenv](https://virtualenv.pypa.io/en/stable/), por padrão já vem instalada no Python 3.6

```bash
python -m venv .ellopolitico
source .ellopolitico/bin/activate
pip install -r requirements.pip
```

### Testes
Para a realização dos testes foi utilizada a biblioteca[**pytest**](https://docs.pytest.org/en/latest/)
```bash
pytest . -vvv
```

### Execução
Para inserir os parlamentares no banco de dados é só exucutar o script **run.py**
```bash
python run.py
```

## Questionário

No diretório **doc** contém o PDF com a descrição do projeto e arquivos que fazem parte das respostas.

2)

**a)** Desenvolvimento com fluxo de TDD, garantindo a cobertura de testes automatizado, e como estrutura
seria utilizado como base o design pattern MVC(Model, View, Controller) para melhor dividir
os código em relação as suas responsabilidades. A utilização de um ORM no modelo facilita a elaboração de queries
e tratamento de segurança, como SQL Injection.
Se o escopo do projeto for um sistema web, um framework como Django poderia ser utilizado.

**b)** Os modelos estão descritos no arquivo **doc/Respostas/2/models.py**, utilizando o mínimo informações necessárias.
Um classmethod foi desenvolvido com a finalidade para prover de forma simples uma opção para
consultar o projeto de Lei mais controversos, populares ou neutro.


3) 

Um grande volume de acesso pode acarretar na oscilação, aumento no tempo de resposta ou até na indisponibilidade total do sistema.
Antes de propor uma solução, analises e monitoramentos dos gargalos da aplicação são necessários. Com eles é
possível identificar se o problema está no número de requisições, tempo de resposta do banco, consumo de processamento, entre outros problemas.

Os problemas mais comuns são o tempo de resposta do sistema e quantidade/tempo de queries no banco de dados.
Uma solução seria a adoção de um sistema de Cache, que pode ser utilizado no retorno das queries do banco e também no retorno da página web por completo.
O sistema de cache tem que ser monitorado e bem estruturado de forma que os dados mais antigos sejam deletados e o mais novos adicionados,
assim evitando que o sistema fique com conflito de informação entre o banco e o que é retornado para o cliente.

Para o problema de processamento no banco de dados existem diversas técnicas,
uma delas é criar um server do banco para leitura e outra para escrita e habilitar a opção de sincronia entre eles.
Uma preocupação também seria na criação de index para melhorar o sistema de buscas e rodar queries complexas em background,
armazenando seu resultado em bases temporárias como sqlite.
