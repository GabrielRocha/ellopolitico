import peewee
from peewee import fn


class Base(peewee.Model):

    class Meta:
        database = peewee.SqliteDatabase(":memory:")


class Group(Base):
    name = peewee.CharField()


class User(Base):
    email = peewee.CharField()
    group = peewee.ForeignKeyField(Group)


class Bill(Base):
    title = peewee.CharField()
    owner = peewee.CharField()
    state = peewee.CharField()


class UserPositioning(Base):
    user = peewee.ForeignKeyField(User, related_name="positionings")
    bill = peewee.ForeignKeyField(Bill, related_name="positionings")
    positioning = peewee.CharField(choices=[('Disagree', "Contrário"),
                                            ('Neutral', "Neutro"),
                                            ('Agree', 'Favorável')])

    @classmethod
    def popularity(cls, positioning):
        return cls.select(cls.bill).where(
            cls.positioning == positioning).group_by(
            cls.bill).order_by(fn.COUNT(cls.bill).desc()).limit(1).get().bill


class Subscription(Base):
    user = peewee.ForeignKeyField(User)
    bill = peewee.ForeignKeyField(Bill)


class BillComment(Base):
    user = peewee.ForeignKeyField(User)
    bill = peewee.ForeignKeyField(Bill)
    comment = peewee.TextField()
