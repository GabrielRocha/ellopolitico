from senado.crawler import populate_parlamentares, retrieve_parlamentares


if __name__ == '__main__':
    populate_parlamentares(retrieve_parlamentares())