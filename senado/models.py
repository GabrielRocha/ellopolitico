import peewee
from . import settings


class BaseModel(peewee.Model):

    @classmethod
    def create_or_update(cls, **kwargs):
        try:
            cls.create(**kwargs)
        except peewee.IntegrityError:
            primary_key_field = cls._meta.get_primary_key_fields()[0].name
            primary_key = kwargs.get(primary_key_field)
            cls.update(**kwargs).where(
                getattr(cls, primary_key_field)==primary_key).execute()

    class Meta:
        database = settings.DATABASE


class Legislatura(BaseModel):
    numero = peewee.IntegerField(primary_key=True)
    data_inicio = peewee.DateField()
    data_fim = peewee.DateField()

    @classmethod
    def get_or_create_by_json(cls, legislatura):
        return cls.get_or_create(numero=legislatura['NumeroLegislatura'],
                                 data_inicio=legislatura['DataInicio'],
                                 data_fim=legislatura['DataFim'])[0]


class Mandato(BaseModel):
    codigo = peewee.IntegerField(primary_key=True)
    uf = peewee.CharField()
    primeira_legislatura = peewee.ForeignKeyField(Legislatura,
                                                  related_name='primeira_legislatura')
    segunda_legislatura = peewee.ForeignKeyField(Legislatura,
                                                 related_name='segunda_legislatura')
    descricao_participacao = peewee.CharField()


    @classmethod
    def create_by_json(cls, mandato):
        cls.create_or_update(codigo=mandato['CodigoMandato'],
                             uf=mandato['UfParlamentar'],
                             primeira_legislatura=Legislatura.get_or_create_by_json(
                                 mandato['PrimeiraLegislaturaDoMandato']),
                             segunda_legislatura=Legislatura.get_or_create_by_json(
                                 mandato['SegundaLegislaturaDoMandato']),
                             descricao_participacao=mandato['DescricaoParticipacao'])


class Suplente(BaseModel):
    mandato = peewee.ForeignKeyField(Mandato, related_name='suplentes')
    codigo = peewee.IntegerField(primary_key=True)
    descricao = peewee.CharField()
    nome = peewee.CharField()

    @classmethod
    def create_by_json(cls, suplente, mandato):
        cls.create_or_update(mandato=mandato,
                             codigo=suplente['CodigoParlamentar'],
                             descricao=suplente['DescricaoParticipacao'],
                             nome=suplente['NomeParlamentar'])


class Exercicio(BaseModel):
    mandato = peewee.ForeignKeyField(Mandato, related_name='exercicios')
    codigo = peewee.IntegerField(primary_key=True)
    data_inicio = peewee.DateField()
    data_fim = peewee.DateField(null=True)
    data_leitura = peewee.DateField(null=True)
    sigla_causa_afastamento = peewee.CharField(null=True)
    descricao_causa_afastamento = peewee.CharField(null=True)

    @classmethod
    def create_by_json(cls, execicio, mandato):
        atributos = dict(mandato=mandato,
                         codigo=execicio['CodigoExercicio'],
                         data_inicio=execicio['DataInicio'],
                         data_fim=execicio.get('DataFim', None),
                         data_leitura=execicio.get('DataLeitura', None),
                         sigla_causa_afastamento=execicio.get('SiglaCausaAfastamento',None),
                         descricao_causa_afastamento=execicio.get('DescricaoCausaAfastamento',
                                                                  None))
        cls.create_or_update(**atributos)

    class Meta:
        order_by = ('-codigo', )


class Parlamentar(BaseModel):
    mandato = peewee.ForeignKeyField(Mandato)
    codigo = peewee.IntegerField(primary_key=True)
    nome = peewee.CharField()
    nome_completo = peewee.CharField()
    sexo = peewee.CharField()
    forma_tratamento = peewee.CharField()
    foto_url = peewee.CharField()
    site = peewee.CharField()
    email = peewee.CharField(null=True)
    sigla_partido = peewee.CharField()
    uf = peewee.CharField()

    @classmethod
    def create_by_json(cls, parlamentar, mandato):
        cls.create_or_update(mandato=mandato,
                             codigo=parlamentar['CodigoParlamentar'],
                             nome=parlamentar['NomeParlamentar'],
                             nome_completo=parlamentar['NomeCompletoParlamentar'],
                             sexo=parlamentar['SexoParlamentar'],
                             forma_tratamento=parlamentar['FormaTratamento'],
                             foto_url=parlamentar['UrlFotoParlamentar'],
                             site=parlamentar['UrlPaginaParlamentar'],
                             email=parlamentar.get('EmailParlamentar', None),
                             sigla_partido=parlamentar['SiglaPartidoParlamentar'],
                             uf=parlamentar['UfParlamentar'])
