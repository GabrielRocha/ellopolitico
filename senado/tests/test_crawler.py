import requests_mock
from senado.settings import URL
from .helper import RESPONSE, init_base, PARLAMENTAR
from senado import crawler
from senado.models import (Parlamentar, Exercicio,
                           Legislatura, Suplente, Mandato, BaseModel)
import pytest
import peewee
import datetime


@pytest.fixture
def new_database_connection():
    BaseModel._meta.database = peewee.SqliteDatabase(':memory:')


def test_retrieve_parlamentares():
    with requests_mock.mock() as mock:
        mock.get(URL, json=RESPONSE)
        assert crawler.retrieve_parlamentares() == PARLAMENTAR


@pytest.mark.usefixtures("new_database_connection")
@pytest.mark.parametrize('model', [Parlamentar, Exercicio,
                                   Legislatura, Suplente,
                                   Mandato])
def test_create_databases(model):
    model._meta.database = peewee.SqliteDatabase(':memory:')
    crawler.create_databases()
    assert model.table_exists()


@pytest.mark.usefixtures("new_database_connection")
def test_dont_create_basemodel():
    crawler.create_databases()
    assert BaseModel.table_exists() is False


def test_populate_list_execicios():
    init_base(Legislatura)
    init_base(Mandato)
    Legislatura.create(numero=54,
                       data_inicio="2011-02-01",
                       data_fim="2011-02-01")
    Mandato.create(codigo=12,
                   uf='RJ',
                   primeira_legislatura=54,
                   segunda_legislatura=54,
                   descricao_participacao='Descricao')
    init_base(Exercicio)
    json_exercicios = {"Exercicio": [
        {
            "CodigoExercicio": "2810",
            "DataInicio": "2017-01-14"
        },
        {
            "CodigoExercicio": "2754",
            "DataInicio": "2015-02-01",
            "DataFim": "2016-09-08",
            "SiglaCausaAfastamento": "LSP",
            "DescricaoCausaAfastamento": "Licença Saúde-Particular"
        }
    ]
    }
    crawler.populate_execicios(json_exercicios, 12)
    assert Exercicio.select().count() == 2
    assert Exercicio.get(codigo=2810).data_inicio == datetime.date(2017, 1, 14)
    assert Exercicio.get(codigo=2754).sigla_causa_afastamento == "LSP"


def test_populate_object_execicios():
    init_base(Legislatura)
    init_base(Mandato)
    init_base(Exercicio)
    Legislatura.create(numero=54,
                       data_inicio="2011-02-01",
                       data_fim="2011-02-01")
    Mandato.create(codigo=12,
                   uf='RJ',
                   primeira_legislatura=54,
                   segunda_legislatura=54,
                   descricao_participacao='Descricao')
    json_exercicios = {"Exercicio":
        {
            "CodigoExercicio": "2810",
            "DataInicio": "2017-01-14"
        }
    }
    crawler.populate_execicios(json_exercicios, 12)
    assert Exercicio.select().count() == 1
    assert Exercicio.get(codigo=2810).data_inicio == datetime.date(2017, 1, 14)


def test_populate_list_suplentes():
    init_base(Legislatura)
    init_base(Mandato)
    init_base(Suplente)
    Legislatura.create(numero=54,
                       data_inicio="2011-02-01",
                       data_fim="2011-02-01")
    Mandato.create(codigo=12,
                   uf='RJ',
                   primeira_legislatura=54,
                   segunda_legislatura=54,
                   descricao_participacao='Descricao')
    json_suplentes = {"Suplente": [
        {
            "DescricaoParticipacao": "1º Suplente",
            "CodigoParlamentar": "5615",
            "NomeParlamentar": "Gilberto Piselo"
        },
        {
            "DescricaoParticipacao": "2º Suplente",
            "CodigoParlamentar": "5617",
            "NomeParlamentar": "Pastor Valadares"
        }
    ]
    }
    crawler.populate_suplentes(json_suplentes, 12)
    assert Suplente.select().count() == 2
    assert Suplente.get(codigo=5615).nome == "Gilberto Piselo"
    assert Suplente.get(codigo=5617).nome == "Pastor Valadares"


def test_populate_object_suplentes():
    init_base(Legislatura)
    init_base(Mandato)
    init_base(Suplente)
    Legislatura.create(numero=54,
                       data_inicio="2011-02-01",
                       data_fim="2011-02-01")
    Mandato.create(codigo=12,
                   uf='RJ',
                   primeira_legislatura=54,
                   segunda_legislatura=54,
                   descricao_participacao='Descricao')
    json_suplentes = {"Suplente":
        {
            "DescricaoParticipacao": "1º Suplente",
            "CodigoParlamentar": "5615",
            "NomeParlamentar": "Gilberto Piselo"
        }
    }
    crawler.populate_suplentes(json_suplentes, 12)
    assert Suplente.select().count() == 1
    assert Suplente.get(codigo=5615).nome == "Gilberto Piselo"


def test_populate_parlamentares():
    init_base(Legislatura)
    init_base(Mandato)
    init_base(Suplente)
    init_base(Exercicio)
    init_base(Parlamentar)
    crawler.populate_parlamentares(PARLAMENTAR)
    parlamentar = Parlamentar.get(codigo=4981)
    assert parlamentar.nome == "Acir Gurgacz"
    assert parlamentar.nome_completo == "Acir Marcos Gurgacz"
    assert parlamentar.sexo == "Masculino"
    assert parlamentar.forma_tratamento == "Senador "
    assert parlamentar.foto_url == "http://www.senado.leg.br/senadores/img/fotos-oficiais/senador4981.jpg"
    assert parlamentar.site == "http://www25.senado.leg.br/web/senadores/senador/-/perfil/4981"
    assert parlamentar.email == "acir@senador.leg.br"
    assert parlamentar.sigla_partido == "PDT"
    assert parlamentar.uf == "RO"
    assert parlamentar.mandato.codigo == 515
    assert parlamentar.mandato.uf == "RO"
    assert parlamentar.mandato.primeira_legislatura.numero == 55
    assert parlamentar.mandato.segunda_legislatura.numero == 56
    assert parlamentar.mandato.descricao_participacao == "Titular"
    assert parlamentar.mandato.suplentes.count() == 2
    assert parlamentar.mandato.suplentes[0].nome == "Gilberto Piselo"
    assert parlamentar.mandato.suplentes[1].nome == "Pastor Valadares"
    assert parlamentar.mandato.exercicios[0].codigo == 2810
    assert parlamentar.mandato.exercicios[1].codigo == 2754
