from senado import models
import peewee


RESPONSE = {"ListaParlamentarEmExercicio": {
    "@xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
    "@xsi:noNamespaceSchemaLocation": "http://legis.senado.leg.br/dadosabertos/dados/ListaParlamentarEmExerciciov4.xsd",
    "Metadados": {"Versao": "26/11/2017 15:11:41",
                  "VersaoServico": "4",
                  "DescricaoDataSet": "Lista dos parlamentares que estão atualmente em exercício"},
    "Parlamentares": {
        "Parlamentar": [{
            "IdentificacaoParlamentar": {
                "CodigoParlamentar": "4981",
                "NomeParlamentar": "Acir Gurgacz",
                "NomeCompletoParlamentar": "Acir Marcos Gurgacz",
                "SexoParlamentar": "Masculino",
                "FormaTratamento": "Senador ",
                "UrlFotoParlamentar": "http://www.senado.leg.br/senadores/img/fotos-oficiais/senador4981.jpg",
                "UrlPaginaParlamentar": "http://www25.senado.leg.br/web/senadores/senador/-/perfil/4981",
                "EmailParlamentar": "acir@senador.leg.br",
                "SiglaPartidoParlamentar": "PDT",
                "UfParlamentar": "RO"},
            "Mandato": {
                "CodigoMandato": "515",
                "UfParlamentar": "RO",
                "PrimeiraLegislaturaDoMandato": {
                    "NumeroLegislatura": "55",
                    "DataInicio": "2015-02-01",
                    "DataFim": "2019-01-31"
                },
                "SegundaLegislaturaDoMandato": {
                    "NumeroLegislatura": "56",
                    "DataInicio": "2019-02-01",
                    "DataFim": "2023-01-31"
                },
                "DescricaoParticipacao": "Titular",
                "Suplentes": {
                    "Suplente": [
                        {
                            "DescricaoParticipacao": "1º Suplente",
                            "CodigoParlamentar": "5615",
                            "NomeParlamentar": "Gilberto Piselo"
                        },
                        {
                            "DescricaoParticipacao": "2º Suplente",
                            "CodigoParlamentar": "5617",
                            "NomeParlamentar": "Pastor Valadares"
                        }
                    ]
                },
                "Exercicios": {
                    "Exercicio": [
                        {
                            "CodigoExercicio": "2810",
                            "DataInicio": "2017-01-14"
                        },
                        {
                            "CodigoExercicio": "2754",
                            "DataInicio": "2015-02-01",
                            "DataFim": "2016-09-08",
                            "SiglaCausaAfastamento": "LSP",
                            "DescricaoCausaAfastamento": "Licença Saúde-Particular"
                        }
                    ]
                }
            },
            "UrlGlossario": "http://legis.senado.leg.br/dadosabertos/senador/"
        }]
    }
}}

PARLAMENTAR = [{
        "IdentificacaoParlamentar": {
            "CodigoParlamentar": "4981",
            "NomeParlamentar": "Acir Gurgacz",
            "NomeCompletoParlamentar": "Acir Marcos Gurgacz",
            "SexoParlamentar": "Masculino",
            "FormaTratamento": "Senador ",
            "UrlFotoParlamentar": "http://www.senado.leg.br/senadores/img/fotos-oficiais/senador4981.jpg",
            "UrlPaginaParlamentar": "http://www25.senado.leg.br/web/senadores/senador/-/perfil/4981",
            "EmailParlamentar": "acir@senador.leg.br",
            "SiglaPartidoParlamentar": "PDT",
            "UfParlamentar": "RO"},
        "Mandato": {
            "CodigoMandato": "515",
            "UfParlamentar": "RO",
            "PrimeiraLegislaturaDoMandato": {
                "NumeroLegislatura": "55",
                "DataInicio": "2015-02-01",
                "DataFim": "2019-01-31"
            },
            "SegundaLegislaturaDoMandato": {
                "NumeroLegislatura": "56",
                "DataInicio": "2019-02-01",
                "DataFim": "2023-01-31"
            },
            "DescricaoParticipacao": "Titular",
            "Suplentes": {
                "Suplente": [
                    {
                        "DescricaoParticipacao": "1º Suplente",
                        "CodigoParlamentar": "5615",
                        "NomeParlamentar": "Gilberto Piselo"
                    },
                    {
                        "DescricaoParticipacao": "2º Suplente",
                        "CodigoParlamentar": "5617",
                        "NomeParlamentar": "Pastor Valadares"
                    }
                ]
            },
            "Exercicios": {
                "Exercicio": [
                    {
                        "CodigoExercicio": "2810",
                        "DataInicio": "2017-01-14"
                    },
                    {
                        "CodigoExercicio": "2754",
                        "DataInicio": "2015-02-01",
                        "DataFim": "2016-09-08",
                        "SiglaCausaAfastamento": "LSP",
                        "DescricaoCausaAfastamento": "Licença Saúde-Particular"
                    }
                ]
            }
        },
        "UrlGlossario": "http://legis.senado.leg.br/dadosabertos/senador/"
    }]


class Model(models.BaseModel):
    id = peewee.IntegerField(primary_key=True)
    name = peewee.CharField(null=True)

    class Meta:
        database = peewee.SqliteDatabase(":memory:")


def assert_model(model, field):
    name, _type = field
    assert name in model._meta.fields
    assert isinstance(model._meta.fields[name], _type)


def init_base(model):
    model._meta.database = peewee.SqliteDatabase(':memory:')
    model.drop_table(fail_silently=True)
    model.create_table(fail_silently=True)

