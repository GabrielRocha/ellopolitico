import pytest
import peewee
from senado import models
from senado.tests.helper import assert_model, init_base


def test_length_mandato():
    assert len(models.Parlamentar._meta.fields) == 11


@pytest.mark.parametrize("field", [('mandato', peewee.ForeignKeyField),
                                   ('codigo', peewee.IntegerField),
                                   ('nome', peewee.CharField),
                                   ('nome_completo', peewee.CharField),
                                   ('sexo', peewee.CharField),
                                   ('forma_tratamento', peewee.CharField),
                                   ('foto_url', peewee.CharField),
                                   ('site', peewee.CharField),
                                   ('email', peewee.CharField),
                                   ('sigla_partido', peewee.CharField),
                                   ('uf', peewee.CharField),])
def test_parlamentar_fields(field):
    assert_model(models.Parlamentar, field)


def test_parlamentar_foreignkey():
    foreign_key = models.Parlamentar.mandato.rel_model()
    assert isinstance(foreign_key, models.Mandato)


def test_get_or_create_by_json_parlamentar():
    init_base(models.Legislatura)
    init_base(models.Mandato)
    models.Legislatura.create(numero=54,
                              data_inicio="2011-02-01",
                              data_fim="2011-02-01")
    models.Mandato.create(codigo=12,
                          uf='RJ',
                          primeira_legislatura=54,
                          segunda_legislatura=54,
                          descricao_participacao='Descricao')
    init_base(models.Parlamentar)
    json = {'CodigoParlamentar': 1231,
            'NomeParlamentar': "Name",
            'NomeCompletoParlamentar': "Full Name",
            'SexoParlamentar': "Masculino",
            'FormaTratamento': "senador",
            'UrlFotoParlamentar': "localhost",
            'UrlPaginaParlamentar': "localhost",
            'EmailParlamentar': "user@user.com",
            'SiglaPartidoParlamentar': "ABC",
            'UfParlamentar': "RJ"}
    models.Parlamentar.create_by_json(json, 12)
    parlamentar = models.Parlamentar.get(codigo=1231)
    assert parlamentar.codigo == 1231
    assert parlamentar.mandato.codigo == 12
    assert parlamentar.nome == 'Name'
    assert parlamentar.nome_completo == 'Full Name'
    assert parlamentar.sexo == 'Masculino'
    assert parlamentar.forma_tratamento == 'senador'
    assert parlamentar.foto_url == 'localhost'
    assert parlamentar.site == 'localhost'
    assert parlamentar.email == 'user@user.com'
    assert parlamentar.sigla_partido == 'ABC'
    assert parlamentar.uf == 'RJ'
