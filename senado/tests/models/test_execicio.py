import pytest
import peewee
from senado import models
from senado.tests.helper import assert_model, init_base
import datetime


def test_length_mandato():
    assert len(models.Exercicio._meta.fields) == 7


@pytest.mark.parametrize("field", [('mandato', peewee.ForeignKeyField),
                                   ('codigo', peewee.IntegerField),
                                   ('data_inicio', peewee.DateField),
                                   ('data_fim', peewee.DateField),
                                   ('data_leitura', peewee.DateField),
                                   ('sigla_causa_afastamento', peewee.CharField),
                                   ('descricao_causa_afastamento', peewee.CharField),])
def test_mandato_fields(field):
    assert_model(models.Exercicio, field)


def test_execicio_foreignkey():
    foreign_key = models.Exercicio.mandato.rel_model()
    assert isinstance(foreign_key, models.Mandato)


def test_get_or_create_by_json_exercicio():
    init_base(models.Legislatura)
    init_base(models.Mandato)
    models.Legislatura.create(numero=54,
                              data_inicio="2011-02-01",
                              data_fim="2011-02-01")
    models.Mandato.create(codigo=12,
                             uf='RJ',
                             primeira_legislatura=54,
                             segunda_legislatura=54,
                             descricao_participacao='Descricao')
    models.Exercicio.create_table(fail_silently=True)
    json = {'CodigoExercicio': 1234,
            'DataInicio': "2011-02-01",
            'DataFim': "2011-02-01",
            'SiglaCausaAfastamento': 'XYZ',
            'DescricaoCausaAfastamento': 'Here'}
    models.Exercicio.create_by_json(json, 12)
    exercicio = models.Exercicio.get(codigo=1234)
    assert exercicio.mandato.codigo == 12
    assert exercicio.data_inicio == datetime.date(2011, 2, 1)
    assert exercicio.data_fim == datetime.date(2011, 2, 1)
    assert exercicio.data_leitura is None
    assert exercicio.sigla_causa_afastamento == 'XYZ'
    assert exercicio.descricao_causa_afastamento == 'Here'
