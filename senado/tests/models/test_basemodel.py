import pytest
from senado.tests.helper import Model, init_base


@pytest.fixture
def init_model():
    init_base(Model)


@pytest.mark.usefixtures("init_model")
def test_create_basemodel():
    id = 1
    Model.create_or_update(id=id)
    assert Model.get(id=id)


@pytest.mark.usefixtures("init_model")
def test_update_basemodel():
    id = 1
    Model.create_or_update(id=id, name="One")
    assert Model.get(id=id).name == "One"
    Model.create_or_update(id=id, name="Two")
    assert Model.get(id=id).name == "Two"
