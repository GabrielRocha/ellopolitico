import pytest
import peewee
from senado import models
from senado.tests.helper import assert_model, init_base


def test_length_mandato():
    assert len(models.Mandato._meta.fields) == 5


@pytest.mark.parametrize("field", [('codigo', peewee.IntegerField),
                                   ('uf', peewee.CharField),
                                   ('primeira_legislatura', peewee.ForeignKeyField),
                                   ('segunda_legislatura', peewee.ForeignKeyField),
                                   ('descricao_participacao', peewee.CharField)])
def test_mandato_fields(field):
    assert_model(models.Mandato, field)


@pytest.mark.parametrize('field', ['primeira_legislatura',
                                   'segunda_legislatura'])
def test_mandato_foreignkey(field):
    foreign_key = getattr(models.Mandato, field).rel_model()
    assert isinstance(foreign_key, models.Legislatura)


def test_get_or_create_by_json_mandato():
    init_base(models.Mandato)
    init_base(models.Legislatura)
    json = {"CodigoMandato": "476",
            "UfParlamentar": "RN",
            "PrimeiraLegislaturaDoMandato": {
                "NumeroLegislatura": "54",
                "DataInicio": "2011-02-01",
                "DataFim": "2015-01-31"
            },
            "SegundaLegislaturaDoMandato": {
                "NumeroLegislatura": "55",
                "DataInicio": "2015-02-01",
                "DataFim": "2019-01-31"
            },
            "DescricaoParticipacao": "Titular",
        }
    models.Mandato.create_by_json(json)
    mandato = models.Mandato.get(codigo=476)
    assert mandato.uf == 'RN'
    assert mandato.primeira_legislatura.numero == 54
    assert mandato.segunda_legislatura.numero == 55
