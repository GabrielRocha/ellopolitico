import pytest
import peewee
from senado import models
from senado.tests.helper import assert_model


@pytest.mark.parametrize('field', [('numero', peewee.IntegerField),
                                   ('data_inicio', peewee.DateField),
                                   ('data_fim', peewee.DateField)])
def test_legislatura_fields(field):
    assert_model(models.Legislatura, field)


def test_length_legislatura():
    assert len(models.Legislatura._meta.fields) == 3


def test_get_or_create_by_json_legislatura():
    models.Legislatura.create_table(fail_silently=True)
    json = {'NumeroLegislatura': 1, 'DataInicio': '2009-11-01', 'DataFim': '2009-12-01'}
    models.Legislatura.get_or_create_by_json(json)
    assert models.Legislatura.get(numero=1)
