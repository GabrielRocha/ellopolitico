import pytest
import peewee
from senado import models
from senado.tests.helper import assert_model, init_base


def test_length_mandato():
    assert len(models.Suplente._meta.fields) == 4


@pytest.mark.parametrize("field", [('mandato', peewee.ForeignKeyField),
                                   ('codigo', peewee.IntegerField),
                                   ('descricao', peewee.CharField),
                                   ('nome', peewee.CharField),])
def test_mandato_fields(field):
    assert_model(models.Suplente, field)


def test_mandato_foreignkey():
    foreign_key = models.Suplente.mandato.rel_model()
    assert isinstance(foreign_key, models.Mandato)


def test_get_or_create_by_json_mandato():
    init_base(models.Legislatura)
    init_base(models.Mandato)
    models.Legislatura.create(numero=54,
                              data_inicio="2011-02-01",
                              data_fim="2011-02-01")
    models.Mandato.create(codigo=12,
                             uf='RJ',
                             primeira_legislatura=54,
                             segunda_legislatura=54,
                             descricao_participacao='Descricao')
    models.Suplente.create_table(fail_silently=True)
    json = {'CodigoParlamentar': 345,
            'DescricaoParticipacao': 'Here',
            'NomeParlamentar': 'Nome'}
    models.Suplente.create_by_json(json, 12)
    suplente = models.Suplente.get(codigo=345)
    assert suplente.mandato.codigo == 12
    assert suplente.nome == 'Nome'
    assert suplente.descricao == 'Here'