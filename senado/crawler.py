import requests
from . import settings
from . import models


def retrieve_parlamentares():
    try:
        request_senado = requests.get(settings.URL, headers={"Accept": "application/json"}).json()
        return request_senado['ListaParlamentarEmExercicio']['Parlamentares']['Parlamentar']
    except IndexError:
        raise ValueError('Parlamentares não localizados')


def create_databases():
    for attributes in dir(models):
        model = getattr(models, attributes)
        is_private = not attributes.startswith(("__", 'BaseMode'))
        if is_private and callable(model) and isinstance(model(), models.BaseModel):
            model.create_table(fail_silently=True)


def populate_execicios(exercicios, mandato):
    if exercicios:
        if isinstance(exercicios['Exercicio'], list):
            for execicio in exercicios['Exercicio']:
                models.Exercicio.create_by_json(execicio, mandato)
        else:
            models.Exercicio.create_by_json(exercicios['Exercicio'], mandato)


def populate_suplentes(suplentes, mandato):
    if suplentes:
        if isinstance(suplentes['Suplente'], list):
            for suplente in suplentes['Suplente']:
                models.Suplente.create_by_json(suplente, mandato)
        else:
            models.Suplente.create_by_json(suplentes['Suplente'], mandato)


def populate_parlamentares(parlamentares):
    create_databases()
    for parlamentar in parlamentares:
        mandato = parlamentar['Mandato']
        models.Mandato.create_by_json(mandato)
        populate_execicios(mandato.get('Exercicios', None), mandato['CodigoMandato'])
        populate_suplentes(mandato.get('Suplentes', None), mandato['CodigoMandato'])
        models.Parlamentar.create_by_json(parlamentar['IdentificacaoParlamentar'],
                                          mandato['CodigoMandato'])
